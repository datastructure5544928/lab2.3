public class lab2_03 {
    private static void printArray(int[] nums){
        for (int i =0;i<nums.length;i++){
            System.out.print(nums[i]+" ");
        }
        System.out.println();
    }

    private static void printArray2_3(int[] nums,int count){
        for(int i=0;i<nums.length-count;i++){
            System.out.print(nums[i]+" ");
        }
        for(int j=nums.length-count;j<nums.length;j++){
            System.out.print("_"+" ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] nums = {0,0,1,1,1,2,2,3,3,4};
        int position=0;
              
        for(int i=1;i<nums.length;i++){
            if(nums[i-1]!=nums[i]){  
                nums[position]=nums[i-1];                 
                position++;               
            }
        }
        nums[position]=nums[nums.length-1];
        
        printArray2_3(nums, position+1);
    }
}
